#include"bmp.h"

enum read_status from_bmp( FILE* in, struct image* img ) 
{
    // Header reading
    int64_t header_size = sizeof(struct bmp_header);
    struct bmp_header* header = (struct bmp_header*) malloc(header_size);
    assert(header != NULL);
    if (fread(header, header_size, HEADERS_TO_READ, in) != HEADERS_TO_READ) {
        free(header);
        return READ_INVALID_HEADER;
    }
    
    if (header->bfType != BMP_SIGNATURE || header->biBitCount != BMP_BIT_COUNT) {
        free(header);
        return READ_INVALID_SIGNATURE;
    }

    // Data reading including padding
    uint32_t width = header->biWidth;
    uint32_t height = header->biHeight;
    uint64_t data_size = header->biSizeImage;
    char* bmp_data = (char*) malloc(data_size);
    assert(bmp_data != NULL);
    if (fread(bmp_data, INT8_SIZE, data_size, in) != data_size) {
        free(header);
        free(bmp_data);
        return READ_INVALID_BITS;
    }
    free(header);


    // Delete padding
    uint64_t clean_byte_width = width * sizeof(struct pixel);
    unsigned long padding = (4 - (sizeof(struct pixel) * width) % 4) % 4;
    char* clean_data = (char*) malloc(clean_byte_width * height);
    assert(clean_data != NULL);
    for (int64_t y = 0; y < height; y++) {
        for (int64_t x = 0; x < clean_byte_width; x+=sizeof(struct pixel)) {
            *(clean_data + y * clean_byte_width + x) = *(bmp_data + x + y * (padding + clean_byte_width));
            *(clean_data + y * clean_byte_width + x + 1) = *(bmp_data + x + y * (padding + clean_byte_width) + 1);
            *(clean_data + y * clean_byte_width + x + 2) = *(bmp_data + x + y * (padding + clean_byte_width) + 2);
        }
    }

    *img = build_image(width, height, clean_data);
    free(clean_data);
    free(bmp_data);


    return READ_OK;
}

struct bmp_header* build_bmp_header(struct image const* img) 
{
    struct bmp_header* header = (struct bmp_header*) malloc(sizeof(struct bmp_header));
    assert(header != NULL);
    unsigned long padding = (4 - (sizeof(struct pixel) * img->width) % 4) % 4;
    uint32_t image_size = img->height * (img->width * sizeof(struct pixel)  + padding);
    uint32_t file_size = sizeof(struct bmp_header) + image_size;
    header->bfType = BMP_SIGNATURE;
    header->bfileSize = file_size;
    header->bfReserved = NOT_REVERSED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BMP_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = BMP_PLANES;
    header->biBitCount = BMP_BIT_COUNT;
    header->biCompression = NO_COMPRESSION;
    header->biSizeImage = image_size;
    header->biXPelsPerMeter = 1;
    header->biYPelsPerMeter = 1;
    header->biClrUsed = ALL_COLORS;
    header->biClrImportant = ALL_COLORS;
    return header;
}


enum write_status to_bmp( FILE* out, struct image const* img ) 
{
    uint32_t width = img->width, height = img->height;
    struct bmp_header* header = build_bmp_header(img);
    unsigned long padding = (4 - (sizeof(struct pixel) * img->width) % 4) % 4;

    if (fwrite(header, sizeof(struct bmp_header), 1, out) != 1) {
        free(header);
        return WRITE_ERROR;
    }
    free(header);
    
    for (uint32_t y = 0; y < height; y++) {
        for (uint32_t x = 0; x < width; x++) {
            if (fwrite(img->data + y * width + x, sizeof(struct pixel), 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
        if (fwrite("\0\0\0", padding, 1, out) != 1) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
