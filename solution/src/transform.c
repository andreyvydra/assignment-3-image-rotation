#include"transform.h"

struct image rotate90(struct image* const source) {
    uint64_t height = source->height, width = source->width;
    struct image output = build_empty_image(height, width);
    for (int64_t y = 0; y < height; y++) {
        for (int64_t x = 0; x < width; x++) {
            output.data[y + (width - x - 1) * height] = source->data[y * width + x]; 
        }
    }
    free(source->data);
    return output;
}

struct image rotate180(struct image* const source) {
    uint64_t height = source->height, width = source->width;
    struct image output = build_empty_image(width, height);
    for (int64_t y = 0; y < height; y++) {
        for (int64_t x = 0; x < width; x++) {
            output.data[(height - y - 1) * width + width - x - 1] = source->data[y * width + x]; 
        }
    }
    free(source->data);
    return output;
}

struct image rotate270(struct image* const source) {
    uint64_t height = source->height, width = source->width;
    struct image output = build_empty_image(height, width);
    for (int64_t y = 0; y < height; y++) {
        for (int64_t x = 0; x < width; x++) {
            output.data[height * x + height - y - 1] = source->data[y * width + x]; 
        }
    }
    free(source->data);
    return output;
}
