#include "bmp.h"
#include "image.h"
#include "main.h"
#include "transform.h"

static long get_angle_from_string(char* const str_angle) {
    long angle;
    char *end;
    angle = strtol(str_angle, &end, 10);
    angle = (angle + 360) % 360;
    return angle;
}

static enum read_status load_image_from_bmp(char* const filename, struct image* img) {
    FILE* in = fopen(filename, READ_MODE);
    enum read_status s = from_bmp(in, img);
    if (fclose(in) != 0) {
        return READ_CLOSE_ERROR;
    }
    return s;
}

static enum write_status write_image_to_bmp(char* const filename, struct image* img) {
    FILE* out = fopen(filename, WRITE_MODE);
    enum write_status s = to_bmp(out, img);
    if (fclose(out) != 0) {
        return WRITE_CLOSE_ERROR;
    }
    return s;
}


int main( int argc, char** argv ) {
    if (argc != ARGUMENTS_COUNT) {
        printf("Введено неверное количество элементов");
        return NOT_ENOUGH_ARGUMENTS_CODE;
    }

    struct image* img = malloc(sizeof(struct image));
    assert(img != NULL);
    enum read_status rs = load_image_from_bmp(argv[INPUT_FILE], img);
    if (rs != 0) {
        free(img);
        printf("Ошибка чтения входного файла");
        return READ_FILE_ERROR_CODE;
    }

    long angle = get_angle_from_string(argv[ANGLE]);
    
    if (angle == 90) {
        *img = rotate90(img);
    } else if (angle == 180) {
        *img = rotate180(img);
    } else if (angle == 270) {
        *img = rotate270(img);
    } else if (angle != 0) {
        free_image(img);
        printf("Невалидный угол поворота");
        return INVALIDE_ANGLE_CODE;
    }
    
    enum write_status ws = write_image_to_bmp(argv[OUTPUT_FILE], img);
    if (ws != 0) {
        free_image(img);
        printf("Ошибка записи в файл");
        return READ_FILE_ERROR_CODE;
    }

    free_image(img);

    return 0;
}
