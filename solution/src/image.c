#include "image.h"

struct image build_empty_image(uint64_t const width, uint64_t const heigth)
{
    struct image new_image;
    new_image.width = width;
    new_image.height = heigth;
    new_image.data = (struct pixel*) malloc(sizeof(struct pixel) * width * heigth);
    assert(new_image.data != NULL);
    return new_image;
}

struct image build_image(uint64_t const width, uint64_t const heigth, char* data) {
    struct image new_image = build_empty_image(width, heigth);
    for (int64_t y = 0; y < heigth; y++) {
        for (int64_t x = 0; x < width; x++) {
            struct pixel* p = malloc(sizeof(struct pixel));
            assert(p != NULL);
            p->r = *(data + 2);
            p->g = *(data + 1);
            p->b = *(data);
            new_image.data[y * width + x] = *p;
            data += sizeof(struct pixel);
            free(p);
        }
    }
    return new_image;
}

void free_image(struct image *img) {
    free(img->data);
    free(img);
}
