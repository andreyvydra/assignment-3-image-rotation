#include"image.h"

#ifndef IMAGE_TRANSFORM_H
#define IMAGE_TRANSFORM_H

struct image rotate90( struct image* const source );
struct image rotate180( struct image* const source );
struct image rotate270( struct image* const source );

#endif
