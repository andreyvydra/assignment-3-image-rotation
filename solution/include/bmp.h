#include"image.h"
#include<math.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>

#ifndef BMP_H
#define BMP_H

#define HEADERS_TO_READ 1
#define INT8_SIZE 1
#define BMP_SIGNATURE 0x4D42
#define BMP_SIZE 40
#define NOT_REVERSED 0
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define NO_COMPRESSION 0
#define ALL_COLORS 0

struct  __attribute__((packed)) bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

enum read_status  {
  READ_OK = 0,
  READ_CLOSE_ERROR,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
};

enum read_status from_bmp( FILE* in, struct image* img );

enum  write_status  {
  WRITE_OK = 0,
  WRITE_CLOSE_ERROR,
  WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

struct bmp_header* build_bmp_header(struct image const* img);

#endif
