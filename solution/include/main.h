#include<stdint.h>

#define ARGUMENTS_COUNT 4
#define NOT_ENOUGH_ARGUMENTS_CODE 2
#define INVALIDE_ANGLE_CODE 3
#define READ_FILE_ERROR_CODE 4
#define INPUT_FILE 1
#define OUTPUT_FILE 2
#define ANGLE 3
#define READ_MODE "r"
#define WRITE_MODE "w"

