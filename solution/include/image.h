#include<assert.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

struct __attribute__((packed)) image {
  uint64_t width, height;
  struct pixel* data;
};

struct image build_empty_image(uint64_t const width, uint64_t const heigth);

struct image build_image(uint64_t const width, uint64_t const heigth, char* data);

void free_image(struct image *img);

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

#endif
